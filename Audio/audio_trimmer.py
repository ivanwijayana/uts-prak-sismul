import soundfile as sf
from IPython.display import Audio, display
from pydub import AudioSegment
import os

# Check Folder Original
print("-----------------------------------------------------------------------------------")
print("--------------------------- === AUDIO TRIMMER === ---------------------------------")
print("-----------------------------------------------------------------------------------")
if not os.path.isdir("./original"):
    os.mkdir("./original")
    print("✅ Original folder has been created.")
else:
    print("✅ Original folder exists.")
    

# Check Folder Trimmed
if not os.path.isdir("./trimmed"):
    os.mkdir("./trimmed")
    
    print("✅ Trimmed folder has been created.")
else:
    print("✅ Trimmed folder exists.")
    

# Mengubah waktu dari format menit:detik menjadi detik
def TimeParse(parsedTime):
    min, sec = [int(i) for i in parsedTime.split(":")]
    seconds = (min * 60) + sec
    seconds *= 2
    return seconds



# Trim audio function
def Trim(title, start=None, akhir=None):
    directory = "./original"
    segment = AudioSegment.from_file(os.path.join(directory, title), format="mp3")
    sr = segment.frame_rate
    start = int(sr * start) if start else 0
    akhir = int(sr * akhir) if akhir else len(segment)
    segmentedAudio = segment.get_array_of_samples()
    arraySegment = segmentedAudio[start:akhir]
    trimmedAudio = segment._spawn(arraySegment)
    trimmedAudio.export("./trimmed/" + title, format="mp3")

def Convert(title):
    directory = "./trimmed"
    file_path = os.path.join(directory, title)
    sound = AudioSegment.from_file(file_path)
    
    # Set default bit rate
    bitrate = "192k"
    
    # Determine output format based on input file extension
    output_format = os.path.splitext(title)[1][1:]
    
    converted_path = os.path.splitext(file_path)[0] + "." + output_format
    sound.export(converted_path, format=output_format, bitrate=bitrate)
print("-----------------------------------------------------------------------------------")
title = input("Masukin nama file di folder original kamu !! (Contoh: 'test.mp3')\n")
print("-----------------------------------------------------------------------------------")

start = input("Masukkan waktu awal dalam format [menit]:[detik] (Contoh: '0:00')\n")
startDetik = TimeParse(start) if start else None
print("-----------------------------------------------------------------------------------")

akhir = input("Masukkan waktu akhir dalam format [menit]:[detik] (Contoh: '1:30')\n")
print("-------------------------- Tunggu Yaa, lagi Loading... ----------------------------")

if not akhir:
    audio_info = sf.info("./original/" + title)
    duration_seconds = audio_info.duration
    duration_minutes = int(duration_seconds // 60)
    duration_seconds = int(duration_seconds % 60)
    print("-----------------------------------------------------------------------------------")
    print(f"Panjang lagu sebelum pemotongan: {duration_minutes}:{duration_seconds}")
    akhirDetik = None
else:
    akhirDetik = TimeParse(akhir)

Trim(title, startDetik, akhirDetik)

play_sound = Audio("./trimmed/" + title)
display(play_sound)

# Calculate original duration
audio_info = sf.info("./original/" + title)
original_duration_seconds = audio_info.duration
original_duration_minutes = int(original_duration_seconds // 60)
original_duration_seconds = int(original_duration_seconds % 60)
print("-----------------------------------------------------------------------------------")
print(f"Panjang lagu sebelum pemotongan: {original_duration_minutes}:{original_duration_seconds}")


# Calculate trimmed duration
if akhirDetik:
    trimmed_audio_info = sf.info("./trimmed/" + title)
    trimmed_duration_seconds = trimmed_audio_info.duration
    trimmed_duration_minutes = int(trimmed_duration_seconds // 60)
    trimmed_duration_seconds = int(trimmed_duration_seconds % 60)
    print(f"Panjang lagu setelah pemotongan: {trimmed_duration_minutes}:{trimmed_duration_seconds}")
    print("-----------------------------------------------------------------------------------")

# Compression
Convert(title)