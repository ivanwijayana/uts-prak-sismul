import cv2

# Inisialisasi video capture menggunakan webcam
video_capture = cv2.VideoCapture(0)

# Inisialisasi penghitung frame dan status gerakan
frame_count = 0
motion_detected = False

while True:
    # Membaca frame dari video capture
    ret, frame = video_capture.read()
    
    # Konversi frame menjadi grayscale
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    
    # Menghaluskan frame menggunakan Gaussian Blur
    gray = cv2.GaussianBlur(gray, (21, 21), 0)
    
    # Jika ini frame pertama, simpan frame sebagai background
    if frame_count == 0:
        background = gray
        frame_count += 1
        continue
    
    # Hitung perbedaan absolut antara background dan frame saat ini
    delta = cv2.absdiff(background, gray)
    
    # Thresholding delta untuk mendapatkan gambar biner
    threshold = cv2.threshold(delta, 30, 255, cv2.THRESH_BINARY)[1]
    
    # Menutupi area kecil yang mungkin merupakan noise
    threshold = cv2.dilate(threshold, None, iterations=2)
    
    # Menemukan kontur pada gambar biner
    contours, _ = cv2.findContours(threshold.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    
    # Inisialisasi status gerakan
    motion_detected = False
    
    # Looping melalui kontur dan menggambar kotak bounding pada kontur yang signifikan
    for contour in contours:
        if cv2.contourArea(contour) > 1000:
            (x, y, w, h) = cv2.boundingRect(contour)
            cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 255, 0), 2)
            motion_detected = True
    
    # Menampilkan frame
    cv2.imshow("Motion Detection", frame)
    
    # Menghentikan program jika tombol 'q' ditekan
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

# Melepaskan video capture dan menutup semua jendela
video_capture.release()
cv2.destroyAllWindows()
