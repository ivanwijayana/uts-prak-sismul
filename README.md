# JAWABAN JOB INTERVIEW
# UTS Praktikum Sistem Multimedia
# Image

# No 1. Mampu mendemonstrasikan penggunaan teknologi image processing dan image compression dalam bentuk aplikasi sederhana.

Algoritma Program (Meme Generator):
1. Periksa folder output apakah sudah ada atau belum, jika belum tampilkan pesan "folder belum tersedia" dan buatkan foldernya. Jika sudah ada tampilkan "folder sudah tersedia".

2. Meminta user menginputkan nama file yang akan digenerate menjadi meme

3. Gambar dibaca menggunakan **matplotlib.image.imread()** dan tampilkan gambar menggunakan **pyplot.imshow()** dan **pyplot.show()**

4.	Minta pengguna untuk memasukkan posisi teks ("top", "bottom", atau "both").

5.	Jika posisi adalah "both", panggil fungsi **generate()** dengan teks kosong dan posisi "both".

6.	Jika posisi bukan "both", minta pengguna untuk memasukkan teks meme.

7.	Panggil fungsi **generate()** dengan teks dan posisi yang sesuai. 

8. Buat Fungsi **generate(text, position)** menggunakan parameter teks dan posisi

9. Buka gambar menggunakan **PIL.Image.open()** dan buat objek **ImageDraw** untuk menggambar teks pada gambar.

10. User harus menginputkan ukuran font yang akan digunakan

11. Program menampilkan warna yang tersedia dan user harus menginputkan warna teks yang akan digunakan

12. Mengubah warna font menjadi nilai RGB menggunakan **webcolors,name_to_rgb()** 

13. User harus menginputkan nama bayangan warna yang akan digunakan sesuai yang sudah disediakan dan warna bayangan font diubah menjadi nilai RGB menggunakan **webcolors.name_to_rgb()** 

14. Buat objek menggunakan **ImageFont.truetype()** dengan path dan ukuran font yang sudah di inputkan (user)

15. Jika posisi top / bottom maka  hitung lebar dan tinggi teks menggunakan **ImageDraw.textsize()** dan tentukan posisi teks sesuai pilihan user (misal top / bottom)

16. Gambar teks dengan warna font pada posisi yang telah ditentukan menggunakan ImageDraw.text()

17.	Jika posisi adalah "both", minta user untuk memasukkan teks untuk bagian atas dan bawah gambar.

18.	Hitung lebar dan tinggi teks untuk kedua teks yang dimasukkan menggunakan ImageDraw.textsize() dan tentukan posisi teks untuk bagian atas dan bawah gambar.

19.	Tambahkan efek bayangan pada teks atas dan bawah dengan menghitung posisi bayangan dan gambar teks dengan warna bayangan menggunakan **ImageDraw.text().**

20.	Gambar teks atas dan bawah dengan warna font pada posisi yang telah ditentukan menggunakan** ImageDraw.text().**

21.	Tampilkan gambar hasil meme menggunakan **Image.show().**

22.	Simpan gambar dalam folder "output" dengan nama file yang sesuai dengan teks menggunakan **Image.save().**

23.	Tampilkan pesan bahwa gambar telah disimpan.



## Berikut Ini Demo Program Meme Generator :

![demo-Image](https://gitlab.com/ivanwijayana/uts-prak-sismul/-/raw/master/Image/demo/meme-demo.gif)


# No 2. Mampu menjelaskan cara kerja image processing dan image compression pada aplikasi yang dibuat.

Untuk penjelasan flowchart-nya sebagai berikut :
1. Program akan memeriksa terlebih dahulu apakah folder "output" sudah ada atau belum. Jika belum, program akan otomatis membuatkan foldernya.

2. User harus mengupload gambar yang akan di jadikan meme terlebih dahulu.

3. Gambar yang sudah di upload dibaca menggunakan library **matplotlib.image** dan ditampilkan menggunakan **pyplot.imshow()** dan **pyplot.show()**

4. Program Meminta user memasukan ukuran, warna, dan warna bayangan untuk font

5. Program melakukan akses pada gambar menggunakan library **PIL.Image** dan membuat objek **ImageDraw** untuk menggambar teks pada gambar.

6. Jika user memilih posisi **top** maka program akan menghitung posisi teks pada bagian atas gambar, menambahkan bayangan dan warna teks dengan font yang sesuai.

7. Jika user memilih posisi **bottom** maka program akan menghitung posisi teks pada bagian bawah gambar, menambahkan bayangan dan warna teks dengan font yang sesuai.

8. Jika user memilih posisi **both** (atas & bawah) maka program akan meminta user untuk mengisi teks di bagian **top dan bootm** lalu menghitung posisi teks pada bagian atas dan bawah gambar, menambahkan bayangan dan warna teks dengan font yang sesuai.

9. Gambar hasil meme akan ditampilkan menggunakan library **Image.show()** dan di simpan ke dalam folder output dengan teks yang sesuai di inputkan oleh user menggunakan library **Image.save()**

10. Jika user memilih **both** maka program akan memanggil Fungsi **generate()** dengan teks kosong dan posisi **both**. Jika tidak program akan meminta user untuk menginputkan teks meme dan memanggil Fungsi **generate()** dengan teks dan posisi yang sesuai.


Untuk Konversi menggunakan : 

**pythonCopy code
img.save(save_path, format='JPEG', quality=85, optimize=True)**

Baris code ini menggunakan metode .save() pada objek gambar img untuk menyimpan gambar dalam format JPEG dengan kompresi. Parameter quality=85 mengatur tingkat kualitas kompresi JPEG (dalam rentang 0-100, di mana 100 adalah kualitas terbaik). Parameter optimize=True digunakan untuk mengoptimalkan kompresi dengan menghilangkan data yang tidak perlu dari gambar.

Dengan menggunakan metode .save() ini dengan parameter tersebut, gambar yang dihasilkan akan dikompresi dengan tingkat kualitas 85 dan ukuran file yang lebih kecil dibandingkan dengan citra yang tidak dikompresi.

```mermaid
graph TD
start((Start)) --> A[Check Output Folder] --> |Folder exists| B[Print Output Exists]
B --> C[/Input File Name/]
C --> D{Show Image}
A --> |Folder does not exist| E[Create Output Folder]
E --> B
D --> |Position = 'top'| F[Input Text Top]
D --> |Position = 'bottom'| G[Input Text Bottom]
D --> |Position = 'both'|I
F --> I[Calculate Text Position]
G --> I
I --> J[/Input Text Size/]
J --> K[Add Text To Image]
K --> L[/Input Text Size/]
L --> M[Add Text Size To Image]
M --> N[/Input Color Name/]
N --> O[Add Color To Text]
O --> P[/Input Shadow Color/]
P --> Q[Add Shadow text]
Q --> R{Position text top / bottom }
R --> |TRUE| S[Generate Meme]
R --> |FALSE| RA[Input Teks Top & Bottom]
RA --> S
S --> T[Show Loading Message]
T --> U[Print Save Path]
U --> V[End]


```


# No 3. Mampu menjelaskan dan mendemonstrasikan aspek kecerdasan buatan dalam produk image processing yang dibuat

Pada Program yang saya buat (Meme Generator) terdapat beberapa aspek AI yang terlibat baik secara langsung maupun tidak langsung. Meskipun tidak menggunakan Algoritma Machine Learning yang rumit dan model AI yang kompleks tetapi penggunaanya melibatkan beberapa aspek seperti berinteraksi dengan pengguna, memproses gambar, dan memanipulasi sebuah teks untuk diubah menjadi gambar sehingga menghasilkan meme.
Berikut ini beberapa aspek AI yang terlibat :

1. Pengolahan Citra
Library seperti matplotlib dan PIL (Python Imaging Library) digunakan untuk memanipulasi dan memproses gambar. Ini melibatkan operasi seperti membaca gambar, menampilkan gambar, menggambar teks pada gambar, mengatur ukuran dan posisi teks, dan menyimpan gambar yang sudah diproses.

2. Interaksi pengguna
Program ini menggunakan input() untuk meminta pengguna memasukkan nama file gambar, ukuran font, nama warna font, nama warna bayangan, dan teks meme. Ini memungkinkan interaksi antara program dan pengguna dalam memodifikasi gambar dan menghasilkan meme yang diinginkan.

3. Pemrosesan Bahasa Alami
Pengguna diminta untuk memasukkan teks meme dalam bahasa alami. Teks ini kemudian digunakan dalam proses pembuatan meme dengan menggambar teks pada gambar.

4. Konversi Nama ke Nilai RGB
Library webcolors digunakan untuk mengonversi nama warna yang dimasukkan oleh pengguna menjadi nilai RGB yang dapat digunakan dalam gambar. Fungsi name_to_rgb() melakukan pemetaan antara nama warna dan nilai RGB yang sesuai.



# Audio
# No 1. Mampu mendemonstrasikan penggunaan teknologi audio processing dan audio compression dalam bentuk aplikasi sederhana.

Algoritma Program (Audio Trimmer)
1. Import Semua library yang di butuhkan : soundfile, Ipython.display, pydub, dan os

2. Melakukan pengecekan folder original dan trimmed, jika sudah ada maka akan menampilkan pesan "folder sudah ada". Tetapi jika folder belum ada maka program akan menampilkan pesan "folder belum tersedia" dan otomatis membuatkan keduanya.

3. Mengubah menit : detik menjadi detik menggunakan fungsi **TimeParse**

4. Mendefinisikan fungsi Trim yang digunakan untuk memotong audio. Fungsi ini menerima parameter title (nama file), start (waktu awal dalam detik), dan akhir (waktu akhir dalam detik). Fungsi ini membaca file audio menggunakan AudioSegment, memotong bagian yang diinginkan, dan menyimpan potongan audio yang telah dipotong ke folder "trimmed" dengan nama yang sama.

5.	Mendefinisikan fungsi Convert yang digunakan untuk mengonversi format audio. Fungsi ini menerima parameter title (nama file). Fungsi ini membaca file audio yang telah dipotong menggunakan AudioSegment, menentukan format output berdasarkan ekstensi file input, dan menyimpan file audio yang telah dikonversi dengan format dan bit rate yang ditentukan ke folder "trimmed".

6. user harus menginputkan nama file yang akan di trim pada folder original (cth. nama.extensi) lagu.mp3

7.	user harus menginput waktu awal dalam format "menit:detik" (misalnya, "0:00"). Waktu awal ini akan diubah menjadi detik menggunakan fungsi TimeParse.

8.	user harus menginput waktu akhir dalam format "menit:detik" (misalnya, "1:30"). Jika tidak ada input waktu akhir, maka durasi asli lagu akan digunakan sebagai waktu akhir.

9. memanggil fungsi trim menggunakan parameter untuk melakukan pemotongan audio

10. menampilkan dan memainkan audio hasil trim menggunakan **soundfile.info**

11.	Menghitung durasi asli lagu sebelum pemotongan menggunakan **soundfile.info.**

12.	Jika ada waktu akhir yang ditentukan, menghitung durasi lagu yang telah dipotong menggunakan **soundfile.info** dan menampilkannya.

13.	Mengonversi audio yang telah dipotong ke format yang ditentukan menggunakan fungsi **Convert**.


## Berikut ini link Demo Audio Trimmer :
![demo-Audio](https://gitlab.com/ivanwijayana/uts-prak-sismul/-/raw/master/Audio/demo/audio-demo.gif)

# No 2. Mampu menjelaskan cara kerja audio processing dan audio compression pada aplikasi yang dibuat.

Penjelasan flowchart (Audio Trimmer)
1. Pertama, dilakukan import beberapa modul yang diperlukan, seperti soundfile, IPython.display, pydub, dan os.

2. Kemudian, dilakukan pengecekan folder "Original". Jika folder tersebut tidak ada, maka akan dibuat menggunakan os.mkdir(). Jika sudah ada, pesan "Original folder exists" akan ditampilkan.

3. Selanjutnya, dilakukan pengecekan folder "Trimmed". Jika folder tersebut tidak ada, maka akan dibuat menggunakan os.mkdir(). Jika sudah ada, pesan "Trimmed folder exists" akan ditampilkan.

4. Terdapat fungsi TimeParse(parsedTime) yang digunakan untuk mengubah waktu dari format "menit:detik" menjadi detik. Fungsi ini akan mengembalikan nilai detik yang sudah dikonversi.

5. Ada juga fungsi Trim(title, start=None, akhir=None) yang digunakan untuk memangkas (trim) audio. Fungsi ini menerima tiga parameter: title (nama file audio), start (waktu awal trim), dan akhir (waktu akhir trim). Fungsi ini melakukan beberapa langkah, seperti membaca audio menggunakan AudioSegment.from_file(), menghitung frame rate, mengonversi waktu awal dan akhir menjadi frame, memangkas audio menggunakan indeks array, dan menyimpan audio hasil pemangkasan ke folder "Trimmed" menggunakan trimmedAudio.export().

6. Fungsi Convert(title) digunakan untuk mengonversi audio yang sudah dipangkas menjadi format yang diinginkan. Fungsi ini membaca audio dari folder "Trimmed", menentukan format keluaran berdasarkan ekstensi file, mengonversi audio menggunakan sound.export(), dan menyimpan hasil konversi dengan nama yang sesuai.

7. Dilakukan input dari pengguna untuk memasukkan nama file audio yang ada di folder "Original".

8. Input dari pengguna juga diminta untuk memasukkan waktu awal trim dalam format "menit:detik".

9. Input dari pengguna juga diminta untuk memasukkan waktu akhir trim dalam format "menit:detik".

10. Jika waktu akhir trim tidak diisi (bernilai None), maka dilakukan pengambilan informasi durasi audio menggunakan sf.info(). Durasi audio tersebut kemudian dihitung dalam menit dan detik, dan ditampilkan sebagai "Panjang lagu sebelum pemotongan".

11. Jika waktu akhir trim diisi, maka dilakukan konversi waktu akhir menjadi detik menggunakan fungsi TimeParse(). Durasi audio yang dipangkas kemudian dihitung dalam menit dan detik, dan ditampilkan sebagai "Panjang lagu setelah pemotongan".

12. Audio yang sudah dipangkas kemudian ditampilkan menggunakan IPython.display.Audio().

13. Informasi durasi audio asli (sebelum pemotongan) dihitung menggunakan sf.info(). Durasi audio tersebut kemudian dihitung dalam menit dan detik, dan ditampilkan sebagai "Panjang lagu sebelum pemotongan".

14. Jika waktu akhir trim diisi, dilakukan pengambilan informasi durasi audio hasil pemangkasan menggunakan sf.info(). Durasi audio tersebut kemudian dihitung dalam menit dan detik, dan ditampilkan sebagai "Panjang lagu setelah pemotongan".

15. Terakhir, dilakukan kompresi audio menggunakan fungsi Convert(). Audio yang sudah dipangkas akan diubah formatnya sesuai dengan ekstensi file yang ditentukan, dengan menggunakan bit rate default "192k". Hasil kompresi audio akan disimpan di folder "Trimmed" dengan nama file yang sesuai.

```mermaid
graph TD
start((Start)) --> A[Check Folder Original]
A --> |Folder exists| B[Print Original Folder Created]
B --> C((Input File Name))
C --> D[Check Folder Trimmed]
D --> |Folder exists| E[Print Trimmed Folder Created]
E --> F((Input Start Time))
F --> G{Start Time Provided?}
G -- Yes --> H[TimeParse Function]
G -- No --> I[Start Time = None]
H --> I
I --> J((Input End Time))
J --> K{End Time Provided?}
K -- Yes --> L[TimeParse Function]
K -- No --> M[End Time = None]
M --> N[Trim Function]
N --> O[Convert Function]
O --> P[Display Trimmed Audio]
P --> Q[Calculate Original Duration]
Q --> R[Calculate Trimmed Duration]
R --> S[Convert Function]

```


# No 3. Mampu menjelaskan dan mendemonstrasikan aspek kecerdasan buatan dalam produk audio processing yang dibuat

Pada project audio trimmer yang saya buat ada beberapa aspek AI yang digunakan, berikut ini penjelasannya :

1. soundfile adalah library untuk membaca dan menulis file audio. Dalam kode tersebut, sf.info digunakan untuk mendapatkan informasi tentang file audio seperti durasi dan atribut lainnya.

2. IPython.display adalah library yang digunakan untuk menampilkan audio di dalam notebook. Fungsi Audio digunakan untuk memainkan audio yang telah dipotong.

3. pydub adalah library yang digunakan untuk memanipulasi file audio. Fungsi-fungsi seperti AudioSegment.from_file dan AudioSegment.export digunakan untuk membaca file audio, memotong bagian yang diinginkan, dan menyimpan potongan audio yang telah dipotong ke dalam file baru.

Selain itu, tidak ada aspek kecerdasan buatan yang signifikan dalam program (audio trimmer) yang saya buat. Program ini lebih fokus pada pemrosesan dan manipulasi audio daripada penggunaan algoritma kecerdasan buatan.


