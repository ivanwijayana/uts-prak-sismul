import cv2
from cv2 import aruco
import numpy as np

# Membuat objek dictionary ArUco
aruco_dict = cv2.aruco.Dictionary_get(cv2.aruco.DICT_6X6_250)

# Nomor ID marker yang ingin dibuat
marker_id = 1

# Ukuran gambar marker dalam piksel
image_size = 200

# Membuat gambar marker
marker_image = np.zeros((image_size, image_size), dtype=np.uint8)
marker_image = aruco.drawMarker(aruco_dict, marker_id, image_size, marker_image, 1)

# Menyimpan gambar marker
output_path = 'marker.png'
cv2.imwrite(output_path, marker_image)

print(f"ArUco marker dengan ID {marker_id} telah dibuat dan disimpan sebagai {output_path}.")
