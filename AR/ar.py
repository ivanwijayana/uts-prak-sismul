import cv2
import numpy as np

# Fungsi untuk membaca dan menampilkan video dari webcam
def play_video_from_webcam():
    cap = cv2.VideoCapture(0)  # Menggunakan webcam dengan ID 0

    while True:
        ret, frame = cap.read()
        cv2.imshow('Video', frame)

        if cv2.waitKey(1) == ord('q'):
            break

    cap.release()
    cv2.destroyAllWindows()

# Fungsi untuk mendeteksi dan melacak marker
def detect_marker_from_webcam(marker_path):
    cap = cv2.VideoCapture(0)  # Menggunakan webcam dengan ID 0

    # Membaca file marker
    marker_image = cv2.imread(marker_path)
    marker_size = (100, 100)  # Ukuran marker dalam piksel

    # Membuat detektor marker
    aruco_dict = cv2.aruco.Dictionary_get(cv2.aruco.DICT_6X6_250)
    aruco_params = cv2.aruco.DetectorParameters_create()

    while True:
        ret, frame = cap.read()
        if not ret:
            break

        # Mengubah frame menjadi grayscale
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

        # Mendeteksi marker
        corners, ids, rejected = cv2.aruco.detectMarkers(gray, aruco_dict, parameters=aruco_params)

        if len(corners) > 0:
            # Melacak marker yang terdeteksi
            rvecs, tvecs, _ = cv2.aruco.estimatePoseSingleMarkers(corners, marker_size, cameraMatrix, distCoeffs)

            # Menggambar koordinat dan frame marker
            cv2.aruco.drawDetectedMarkers(frame, corners, ids)
            for i in range(len(ids)):
                cv2.aruco.drawAxis(frame, cameraMatrix, distCoeffs, rvecs[i], tvecs[i], 0.1)

        cv2.imshow('AR', frame)
        if cv2.waitKey(1) == ord('q'):
            break

    cap.release()
    cv2.destroyAllWindows()

# Main program
if __name__ == '__main__':
    # Path marker
    marker_path = 'luffy.jpg'

    # Kamera matrix dan distCoeffs (Anda perlu mengganti nilai-nilai ini dengan nilai yang sesuai)
    cameraMatrix = np.array([[1000, 0, 320], [0, 1000, 240], [0, 0, 1]])
    distCoeffs = np.zeros((4, 1))

    # Memainkan video dari webcam
    play_video_from_webcam()

    # Deteksi dan pelacakan marker dari webcam
    detect_marker_from_webcam(marker_path)
