from matplotlib import image
from matplotlib import pyplot as plt
from PIL import Image, ImageDraw, ImageFont, ImageFilter
import os
from webcolors import name_to_rgb

# Check Output Folder
if os.path.isdir("output"):
    print("-----------------------------------------------------------------------------------")
    print("--------------------------- === MEME GENERATOR === --------------------------------")
    print("-----------------------------------------------------------------------------------")
    print("✅ Folder Output exists.")
    print("-----------------------------------------------------------------------------------")
else:
    print("🚩 Folder Output doesn't exist. Making folder ...")
    os.mkdir('./output')
    print("✅ Folder Output has been created.")
    print("-----------------------------------------------------------------------------------")

filename = input("Coba Masukin Nama File Image-nya !(cth, test.jpg): ")
print("-----------------------------------------------------------------------------------")
data = image.imread(filename)
plt.imshow(data)
plt.show()

def generate(text, position):
    img = Image.open(filename)
    draw = ImageDraw.Draw(img)

    font_path = "Impact.ttf"  # Path to Impact font file
    font_size = int(input("Enter the font size: "))
    print("-----------------------------------------------------------------------------------")

    # Show available color names
    print("Available Color Names:")
    print("black")
    print("white")
    print("red")
    print("-----------------------------------------------------------------------------------")

    font_color_name = input("Coba Input Warna Teks-nya !! (cth, black, white, red): ")
    font_color_rgb = name_to_rgb(font_color_name)
    font_color = tuple(font_color_rgb)
    print("-----------------------------------------------------------------------------------")
    shadow_color_name = input("Masukin Warna Efek Shadow !! (cth, black, white, red): ")
    
    shadow_color_rgb = name_to_rgb(shadow_color_name)
    shadow_color = tuple(shadow_color_rgb)

    font = ImageFont.truetype(font_path, font_size)

    if position == 'top':
        text_width, text_height = draw.textsize(text, font=font)
        text_position = ((img.width - text_width) // 2, 10)

         # Nambahin Shadow Pada Teks
        shadow_offset = 2
        shadow_position = (
            text_position[0] + shadow_offset,
            text_position[1] + shadow_offset,
        )
        draw.text(shadow_position, text, font=font, fill=shadow_color)

        draw.text(text_position, text, font=font, fill=font_color)
    elif position == 'bottom':
        text_width, text_height = draw.textsize(text, font=font)
        text_position = ((img.width - text_width) // 2, img.height - text_height - 10)

         # Nambahin Shadow Pada Teks Bottom
        shadow_offset = 2
        shadow_position = (
            text_position[0] + shadow_offset,
            text_position[1] + shadow_offset,
        )
        draw.text(shadow_position, text, font=font, fill=shadow_color)

        draw.text(text_position, text, font=font, fill=font_color)
    elif position == 'both':
        text_input_top = input("Input Meme Text (top):\n")
        print("-----------------------------------------------------------------------------------")
        text_input_bottom = input("Input Meme Text (bottom):\n")
        text_width_top, text_height_top = draw.textsize(text_input_top, font=font)
        text_width_bottom, text_height_bottom = draw.textsize(text_input_bottom, font=font)
        text_position_top = ((img.width - text_width_top) // 2, 10)
        text_position_bottom = ((img.width - text_width_bottom) // 2, img.height - text_height_bottom - 10)

         # Nambahin Shadow Pada Top Teks
        shadow_offset = 2
        shadow_position_top = (
            text_position_top[0] + shadow_offset,
            text_position_top[1] + shadow_offset,
        )
        draw.text(shadow_position_top, text_input_top, font=font, fill=shadow_color)

        # Nambahin Shadow Pada Teks
        shadow_position_bottom = (
            text_position_bottom[0] + shadow_offset,
            text_position_bottom[1] + shadow_offset,
        )
        draw.text(shadow_position_bottom, text_input_bottom, font=font, fill=shadow_color)

        draw.text(text_position_top, text_input_top, font=font, fill=font_color)
        draw.text(text_position_bottom, text_input_bottom, font=font, fill=font_color)

    print("-----------------------------------------------------------------------------------")
    print("--------------- TUNGGU BENTAR YAAA, LAGI LOADING HEHEHE . . . -----------------")
    print("-----------------------------------------------------------------------------------")


    img.show()
    print("\n")
    save_path = "output/" + os.path.splitext(filename)[0] + " - meme.jpg"
    img.save(save_path, format='JPEG', quality=85, optimize=True)
    print("Image saved in", save_path)

positionInput = input("Enter the position ('top', 'bottom', or 'both'):\n")
print("-----------------------------------------------------------------------------------")
if positionInput == 'both':
    generate('', positionInput)
else:
    textInput = input("Input Meme Text:\n")
    print("-----------------------------------------------------------------------------------")
    generate(textInput, positionInput)
